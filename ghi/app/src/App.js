import { BrowserRouter, Routes, Route, Outlet } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListTechnicians from "./service/ListTechnicians";
import TechnicianForm from "./service/CreateTechnician";
import ListAppointments from "./service/ListAppointments";
import AppointmentFrom from "./service/CreateServiceAppointment";
import ServiceHistory from "./service/ServiceHistory";
import ListManufacturers from "./inventory/ListManufacturers";
import ManufacturerForm from "./inventory/CreateManufacturer";
import AutomobilesList from "./inventory/ListAutomobiles.js";
import AutomobileForm from "./inventory/CreateAutomobile";
import ListModels from "./inventory/ListModels";
import ModelForm from "./inventory/CreateModel";
import CustomerForm from './sales/CustomerForm';
import SalesPersonForm from './sales/SalesPersonForm';
import SalesList from './sales/SalesList';
import SalesForm from './sales/SalesForm';
import CustomerList from './sales/CustomerList';
import SalesPeopleList from './sales/SalesPeople';
import SalesHistory from './sales/SalesHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="customers/*" element={<Outlet />}>
            <Route path="" element={<CustomerList/>} />
            <Route path="new/" element={<CustomerForm />} />
          </Route>
          <Route path="salespeople/*" element={<Outlet />}>
            <Route path="" element={<SalesPeopleList />} />
            <Route path="new/" element={<SalesPersonForm />} />
          </Route>
          <Route path="sales/*" element={<Outlet />}>
            <Route path="" element={<SalesList />} />
            <Route path="new/" element={<SalesForm />} />
            <Route path="history/" element={<SalesHistory />} />
          </Route>
					<Route path="/technicians/" element={<ListTechnicians />} />
					<Route path="/technicians/create/" element={<TechnicianForm />} />
          <Route path="/appointments/" element={<ListAppointments />} />
          <Route path="/appointments/create/" element={<AppointmentFrom />} />
          <Route path="/appointments/history/" element={<ServiceHistory />} />
          <Route path="/manufacturers/" element={<ListManufacturers />} />
          <Route path="/manufacturers/create/" element={<ManufacturerForm />} />
          <Route path="/automobiles/" element={<AutomobilesList />} />
          <Route path="/automobiles/create/" element={<AutomobileForm />} />
          <Route path="/models/" element={<ListModels />} />
          <Route path="/models/create/" element={<ModelForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
