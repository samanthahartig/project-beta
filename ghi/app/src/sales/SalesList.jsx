import React, { useEffect, useState } from 'react';


function SalesList() {
  const [salesRecords, setSalesRecords] = useState([]);

  const onDelete = async (id) => {
    if (window.confirm('Are you sure you want to delete this record?')) {
        const deleteUrl = `http://localhost:8090/api/sales/${id}`;
        const deleteResponse = await fetch(deleteUrl, { method: 'DELETE' });

        if (deleteResponse.ok) {
            setSalesRecords(salesRecords.filter(salesRecord => salesRecord.id !== id));
        }
    }
};

  const getAll = async () => {
    const salesUrl = 'http://localhost:8090/api/sales/';
    const salesResponse = await fetch(salesUrl);

    if (salesResponse.ok) {
      const salesData = await salesResponse.json();
      setSalesRecords(salesData.sales_records);
    }
  };

  useEffect(() => {
    getAll();
  }, []);


  return (
    <div className="container">
      <h1>Sales Records</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Sales person</th>
            <th>Employee number</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Sale price</th>
            <th>Remove Sale</th>
          </tr>
        </thead>
        <tbody>
          {salesRecords
            .map((salesRecord) => (
              <tr key={salesRecord.id}>
                <td>{salesRecord.sales_person.first_name + " "+ salesRecord.sales_person.last_name}</td>
                <td>{salesRecord.sales_person.employee_number}</td>
                <td>{salesRecord.customer.first_name + " "+ salesRecord.customer.last_name}</td>
                <td>{salesRecord.automobile.vin}</td>
                <td>
                  <span>$</span>
                  {salesRecord.price}
                </td>
                <td>
                  <button onClick={() => onDelete(salesRecord.id)}>Delete</button>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
}

export default SalesList
