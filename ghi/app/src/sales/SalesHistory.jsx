import React, { useEffect, useState } from 'react';


function SalesHistory() {
  const [load, setLoad] = useState(false);
  const [salesRecords, setSalesRecords] = useState([]);
  const [filter, setFilter] = useState('');

  const getAll = async () => {
    const salesUrl = 'http://localhost:8090/api/sales/';
    const salesResponse = await fetch(salesUrl);

    if (salesResponse.ok) {
      const salesData = await salesResponse.json();
      setSalesRecords(salesData.sales_records);
    }

    if (true) {
      setLoad(!load);
    }
  };

  useEffect(() => {
    getAll();
  }, [load]);


  const salesPersons = [...new Set(salesRecords.map(record => record.sales_person.first_name + " " + record.sales_person.last_name))];

  const handleFilterChange = (event) => {
    setFilter(event.target.value);
  }


  return (
    <div className="container">
      <h1>Sales Records</h1>
      <div className="mb-3">
        <label>Filter by Sales Person: </label>
        <select onChange={handleFilterChange}>
          <option value="">All</option>
          {salesPersons.map((person, index) => (
            <option key={index} value={person}>{person}</option>
          ))}
        </select>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Sales person</th>
            <th>Employee number</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Sale price</th>
          </tr>
        </thead>
        <tbody>
          {salesRecords
            .filter(record => filter === '' || (record.sales_person.first_name + " " + record.sales_person.last_name) === filter)
            .map((salesRecord) => (
              <tr key={salesRecord.id}>
                <td>{salesRecord.sales_person.first_name + " "+ salesRecord.sales_person.last_name}</td>
                <td>{salesRecord.sales_person.employee_number}</td>
                <td>{salesRecord.customer.first_name + " "+ salesRecord.customer.last_name}</td>
                <td>{salesRecord.automobile.vin}</td>
                <td>
                  <span>$</span>
                  {salesRecord.price}
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
}

export default SalesHistory
