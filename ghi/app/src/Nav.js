import React from 'react';
import { NavLink } from 'react-router-dom';
import './ghi.css';


function Nav() {
  const generateDropdownItems = (items) => {
    return items.map((item) => (
      <li key={item.to}>
        <NavLink className="dropdown-item" to={item.to}>
          {item.label}
        </NavLink>
      </li>
    ));
  };

const salesItems = [
  { to: '/salespeople/', label: 'Sales Persons' },
  { to: '/sales/', label: 'Sales Records' },
  { to: '/sales/history/', label: 'Salesperson History' },
  { to: '/customers/', label: 'Customers' },
];

const salesAdmin = [
  { to: '/salespeople/new/', label: 'Add Sales Person' },
  { to: '/sales/new/', label: 'Add Sales Record' },
  { to: '/customers/new/', label: 'Add Customer' },
];

const inventoryItems = [
  { to: '/manufacturers/', label: 'Manufacturers' },
  { to: '/manufacturers/create/', label: 'Create Manufacturer' },
  { to: '/models/', label: 'Models' },
  { to: '/models/create/', label: 'Create Model' },
  { to: '/automobiles/', label: 'Automobiles' },
  { to: '/automobiles/create/', label: 'Create Automobile' },
];

const serviceItems = [
  { to: '/appointments/', label: 'Appointments' },
  { to: '/appointments/create/', label: 'Create Appointment' },
  { to: '/appointments/history/', label: 'Appointments History' },
  { to: '/technicians/', label: 'Technicians' },
  { to: '/technicians/create/', label: 'Create Technician' },
];


return (
  <nav className="navbar navbar-expand-lg navbar-custom">
    <div className="container-fluid">
      <NavLink className="navbar-brand nav-link-custom" to="/">
        CarCar
      </NavLink>
      <button
        className="navbar-toggler navbar-toggler-custom"
        type="button"
        data-bs-toggle="collapse"
        data-bs-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item dropdown">
            <div className="btn btn-custom dropdown-toggle" id="dropdownInventory" data-bs-toggle="dropdown">
              Inventory
            </div>
            <ul className="dropdown-menu" aria-labelledby="dropdownInventory">
              {generateDropdownItems(inventoryItems)}
            </ul>
          </li>
          <li className="nav-item dropdown">
            <div className="btn btn-custom dropdown-toggle" id="dropdownService" data-bs-toggle="dropdown">
              Service
            </div>
            <ul className="dropdown-menu" aria-labelledby="dropdownService">
              {generateDropdownItems(serviceItems)}
            </ul>
          </li>
          <li className="nav-item dropdown">
            <div className="btn btn-custom dropdown-toggle" id="dropdownSales" data-bs-toggle="dropdown">
              Sales Info
            </div>
            <ul className="dropdown-menu" aria-labelledby="dropdownSales">
              {generateDropdownItems(salesItems)}
            </ul>
          </li>
          <li className="nav-item dropdown">
            <div className="btn btn-custom dropdown-toggle" id="dropdownSales" data-bs-toggle="dropdown">
              Sales Admin
            </div>
            <ul className="dropdown-menu" aria-labelledby="dropdownSales">
              {generateDropdownItems(salesAdmin)}
            </ul>
          </li>

        </ul>
      </div>
    </div>
  </nav>
);
}

export default Nav
