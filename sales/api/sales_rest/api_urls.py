from django.urls import path
from .api_views import (
    api_customers,
    api_customer,
    api_sales_persons,
    api_sales_person,
    api_sales_records,
    api_sales_record,
    list_automobiles,
)


urlpatterns = [
    path("salespeople/", api_sales_persons, name="api_sales_persons"),
    path("salespeople/<int:id>/", api_sales_person, name="api_sales_person"),

    path("customers/", api_customers, name="api_customers"),
    path("customers/<int:id>/", api_customer, name="api_customer"),

    path("sales/", api_sales_records, name="api_sales_records"),
    path("sales/<int:id>/", api_sales_record, name="api_sales_record"),

    path("list_autos/", list_automobiles, name="list_autos"),
]
