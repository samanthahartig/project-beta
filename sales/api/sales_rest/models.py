from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    href = models.CharField(max_length=100, unique=True)
    sold = models.BooleanField(default=False)


class Salesperson(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    employee_number = models.PositiveIntegerField()

    def get_api_url(self):
        return reverse("api_sales_persons", kwargs={"id": self.id})

    #def __str__(self):
        #return f"{self.name}"


class Customer(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    address = models.CharField(max_length=150)
    phone_number = models.CharField(max_length=20)

    def get_api_url(self):
        return reverse("api_customers", kwargs={"id": self.id})

    def __str__(self):
        return f"{self.name}"


class Sale(models.Model):
    price = models.CharField(max_length=150)
    sales_person = models.ForeignKey(
        Salesperson,
        related_name="sales_record",
        on_delete=models.PROTECT,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sales_record",
        on_delete=models.PROTECT,
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales_record",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_sales_records", kwargs={"id": self.id})

    def __str__(self):
        return f"{self.id}"
