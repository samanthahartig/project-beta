import React, { useState } from 'react';
import {useNavigate} from 'react-router-dom';


function SalesPersonForm () {
    const navigate = useNavigate();

    const [first_name, setFirstName] = useState('');
    const handleFirstNameChange = (event) => setFirstName(event.target.value);

    const [last_name, setLastName] = useState('');
    const handleLastNameChange = (event) => setLastName(event.target.value);

    const [employeeNumber, setEmployeeNumber] = useState('');
    const handleEmployeeNumberChange = (event) => setEmployeeNumber(event.target.value);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_number = employeeNumber;

        const customerUrl = 'http://localhost:8090/api/salespeople/';
        const fetchConfig ={
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        try {
            const response = await fetch(customerUrl, fetchConfig);

            if (response.ok) {
              setFirstName('');
              setLastName('');
              setEmployeeNumber('');

              navigate("/salespeople/");

            } else {
            }

          } catch (error) {
          }
        };


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new sales person</h1>
                    <form onSubmit={handleSubmit} id="create-customer-form">
                        <div className="form-floating mb-3">
                            <input
                            onChange={handleFirstNameChange}
                            placeholder="FirstName"
                            required
                            type="text"
                            name="first_name"
                            id="first_name"
                            value={first_name}
                            className="form-control"/>
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                            onChange={handleLastNameChange}
                            placeholder="LastName"
                            required
                            type="text"
                            name="last_name"
                            id="last_name"
                            value={last_name}
                            className="form-control"/>
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleEmployeeNumberChange}
                                placeholder="Employee Number"
                                required
                                type="number"
                                name="employee_number"
                                id="employee_number"
                                value={employeeNumber}
                                className="form-control"/>
                            <label htmlFor="employee_number">Employee Number</label>
                            </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesPersonForm
