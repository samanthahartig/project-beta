import React, { useEffect, useState } from 'react';


function SalesPeopleList() {
    const [sales_persons, setCustomers] = useState([]);

    const onDelete = async (id) => {
        if (window.confirm('Are you sure you want to delete this record?')) {
            const deleteUrl = `http://localhost:8090/api/salespeople/${id}`;
            const deleteResponse = await fetch(deleteUrl, { method: 'DELETE' });

            if (deleteResponse.ok) {
                setCustomers(sales_persons.filter(sales_person => sales_person.id !== id));
            }
        }
    };

    const getAll = async () => {
        const customerUrl = 'http://localhost:8090/api/salespeople/';
        const customerResponse = await fetch(customerUrl);

        if (customerResponse.ok) {
            const customerData = await customerResponse.json();
            setCustomers(customerData.sales_persons);
        }
    };

    useEffect(() => {
        getAll();
    }, []);


    return (
        <div className="container">
            <h1>Sales Persons</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee Number</th>
                        <th>Remove Salesperson</th>
                    </tr>
                </thead>
                <tbody>
                    {sales_persons.map((sales_person) => {
                        return (
                            <tr key={sales_person.id}>
                                <td>{sales_person.first_name}</td>
                                <td>{sales_person.last_name}</td>
                                <td>{sales_person.employee_number}</td>
                                <td>
                                    <button onClick={() => onDelete(sales_person.id)}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default SalesPeopleList
